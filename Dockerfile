FROM node:12

ENV NODE_ENV production

WORKDIR /usr/src/app

COPY ./package*.json ./

RUN npm install --only=production

EXPOSE 3777

CMD [ "npm", "start" ]