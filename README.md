#Welcome to Redstee endpoint
This is endpoint for Redstee which will connect to your redis server 
and then communicate with Redstee app to allow you to browse redis databases in nice web UI.

You can run this nodejs endpoint like any other nodejs apps by simply run app/redsteeEndpoint.js 
(or app/redsteeEndpointSSL.js if you want SSL version).

Repository includes docker-compose file for easy use with Docker. 
If you will use this project in Docker remember to set correct network 
so your redis will be visible to this app.

For SSL version provide valid certificate files. 

Before you start create app/endpoint.json file based on endpoint-example.json (without comments).

**Most important file here is endpoints.json** - 
in this file you need to specify your redis host and port 
as well as you can disable some redis commands so it will never be executed by this up 
(for example you may disabled del or set on production servers).