const redis = require('redis');
const fs = require('fs');
const infoCommand = require('./commands/info');
const selectCommand = require('./commands/select');
const scanCommand = require('./commands/scan');
const existsCommand = require('./commands/exists');
const typeCommand = require('./commands/type');
const ttlCommand = require('./commands/ttl');
const delCommand = require('./commands/del');
const getCommand = require('./commands/get');
const llenCommand = require('./commands/llen');
const lrangeCommand = require('./commands/lrange');
const sscanCommand = require('./commands/sscan');
const zcardCommand = require('./commands/zcard');
const zrangebyscoreCommand = require('./commands/zrangebyscore');
const zcountCommand = require('./commands/zcount');
const hscanCommand = require('./commands/hscan');
const hgetCommand = require('./commands/hget');
const hdelCommand = require('./commands/hdel');
const setCommand = require('./commands/set');
const sremCommand = require('./commands/srem');
const zremCommand = require('./commands/zrem');
const hsetCommand = require('./commands/hset');
const lpushCommand = require('./commands/lpush');
const rpushCommand = require('./commands/rpush');
const saddCommand = require('./commands/sadd');
const zaddCommand = require('./commands/zadd');
const strlenCommand = require('./commands/strlen');
const hlenCommand = require('./commands/hlen');
const scardCommand = require('./commands/scard');
const expireatCommand = require('./commands/expireat');
const commandCommand = require('./commands/command');

module.exports = function commands(req, res) {

    const host = req.body.host;
    const port = req.body.port;
    const commands = req.body.commands;


    const client = redis.createClient({
        host: host,
        port: port
    });

    const endpoints = JSON.parse(fs.readFileSync('./endpoints.json', 'utf8'));

    let finalResult = [];

    function ifCommandIsAllowed(command) {
        for (let endpoint of endpoints) {
            if (endpoint.host === host && endpoint.port === port) {
                if (endpoint.hasOwnProperty('availableCommands')) {
                    let foundCommand = endpoint.availableCommands.find((availableCommand) => {
                        return availableCommand === command;
                    });
                    if (foundCommand) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    async function runCommands(commands) {
        if (commands.length > 0) {
            for (let commandObj of commands) {
                const command = commandObj.command;
                const args = commandObj.args;
                if (!ifCommandIsAllowed(command)) {
                    finalResult.push('__ForbiddenCommand__');
                    continue;
                }
                switch (command) {
                    case 'info':
                        await infoCommand(client, finalResult, args);
                        break;
                    case 'select':
                        await selectCommand(client, finalResult, args);
                        break;
                    case '__scan':
                        await scanCommand(client, finalResult, args);
                        break;
                    case 'exists':
                        await existsCommand(client, finalResult, args);
                        break;
                    case 'type':
                        await typeCommand(client, finalResult, args);
                        break;
                    case 'ttl':
                        await ttlCommand(client, finalResult, args);
                        break;
                    case 'del':
                        await delCommand(client, finalResult, args);
                        break;
                    case 'get':
                        await getCommand(client, finalResult, args);
                        break;
                    case 'llen':
                        await llenCommand(client, finalResult, args);
                        break;
                    case 'lrange':
                        await lrangeCommand(client, finalResult, args);
                        break;
                    case '__sscan':
                        await sscanCommand(client, finalResult, args);
                        break;
                    case 'zcard':
                        await zcardCommand(client, finalResult, args);
                        break;
                    case 'zrangebyscore':
                        await zrangebyscoreCommand(client, finalResult, args);
                        break;
                    case 'zcount':
                        await zcountCommand(client, finalResult, args);
                        break;
                    case '__hscan':
                        await hscanCommand(client, finalResult, args);
                        break;
                    case 'hget':
                        await hgetCommand(client, finalResult, args);
                        break;
                    case 'hdel':
                        await hdelCommand(client, finalResult, args);
                        break;
                    case 'srem':
                        await sremCommand(client, finalResult, args);
                        break;
                    case 'zrem':
                        await zremCommand(client, finalResult, args);
                        break;
                    case 'set':
                        await setCommand(client, finalResult, args);
                        break;
                    case 'hset':
                        await hsetCommand(client, finalResult, args);
                        break;
                    case 'lpush':
                        await lpushCommand(client, finalResult, args);
                        break;
                    case 'rpush':
                        await rpushCommand(client, finalResult, args);
                        break;
                    case 'sadd':
                        await saddCommand(client, finalResult, args);
                        break;
                    case 'zadd':
                        await zaddCommand(client, finalResult, args);
                        break;
                    case 'strlen':
                        await strlenCommand(client, finalResult, args);
                        break;
                    case 'hlen':
                        await hlenCommand(client, finalResult, args);
                        break;
                    case 'scard':
                        await scardCommand(client, finalResult, args);
                        break;
                    case 'expireat':
                        await expireatCommand(client, finalResult, args);
                        break;
                    case '__command':
                        await commandCommand(client, finalResult, args);
                        break;
                }
            }
        }
    }

    runCommands(commands).then(() => {
        client.quit();
        res.json(finalResult);
    });
};
