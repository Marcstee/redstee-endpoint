const fs = require('fs');
module.exports = function endPointInfo(req, res) {
    const endpoints = JSON.parse(fs.readFileSync('./endpoints.json', 'utf8'));
    res.json(endpoints);
};
