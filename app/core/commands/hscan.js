const {promisify} = require('util');
module.exports = async function hscanCommand(client, finalResult, args) {
    const commandAsync = promisify(client.hscan).bind(client);

    let cursor = null;
    const key = args[0];
    const match = args[1];
    const maxItems = args[2];
    const count = 10000;
    let keys = {};
    let isMore = false;

    function addKeys(rawKeys) {
        for (let i = 0; i < rawKeys.length; i++) {
            keys[rawKeys[i]] = rawKeys[++i];
        }
    }

    async function doScan() {
        if (cursor === null) {
            cursor = '0';
        }
        const ret = await commandAsync(key, cursor, 'MATCH', match, 'COUNT', count);
        cursor = ret[0];
        return ret[1];
    }

    while (Object.keys(keys).length <= maxItems && cursor !== '0') {
        addKeys(await doScan());
    }
    if (cursor !== '0'){
        isMore = true;
    }
    if  (Object.keys(keys).length > maxItems){
        const reducedKeys = {};
        let counter = 0;
        for (let key in keys) {
            if (counter >= maxItems){
                break;
            }
            if (keys.hasOwnProperty(key)){
                reducedKeys[key] = keys[key];
                counter++;
            }
        }
        keys = reducedKeys;
        isMore = true;
    }
    finalResult.push({
        list: keys,
        isMore
    });
};

