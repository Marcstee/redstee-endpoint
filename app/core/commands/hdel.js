const {promisify} = require('util');
module.exports = async function hdelCommand(client, finalResult, args) {
    const commandAsync = promisify(client.hdel).bind(client);
    const key = args[0];
    const field = args[1];
    const ret = await commandAsync(key,field);
    finalResult.push(ret === 1);
};