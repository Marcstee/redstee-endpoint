const {promisify} = require('util');
module.exports = async function zaddCommand(client, finalResult, args) {
    const commandAsync = promisify(client.zadd).bind(client);
    const key = args[0];
    const score = args[1];
    const value = args[2];
    const ret = await commandAsync(key,score, value);
    finalResult.push(true);
};