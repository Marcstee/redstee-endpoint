const {promisify} = require('util');
module.exports = async function zrangebyscoreCommand(client, finalResult, args) {
    const commandAsync = promisify(client.zrangebyscore).bind(client);
    const key = args[0];
    const min = args[1];
    const max = args[2];
    const offset = args[3];
    const count = args[4];
    const ret = await commandAsync(key, min, max, 'WITHSCORES', 'LIMIT', offset, count);
    const finalArray = [];
    for (let i = 0; i < ret.length; i++) {
        finalArray.push({
            value: ret[i],
            score: ret[++i]
        });
    }
    finalResult.push(finalArray);
};