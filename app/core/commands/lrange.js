const {promisify} = require('util');
module.exports = async function lrangeCommand(client, finalResult, args) {
    const commandAsync = promisify(client.lrange).bind(client);
    const key = args[0];
    const start = args[1];
    const stop = args[2];
    const ret = await commandAsync(key,start,stop);
    finalResult.push(ret);
};