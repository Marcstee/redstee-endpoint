const {promisify} = require('util');
module.exports = async function hlenCommand(client, finalResult, args) {
    const commandAsync = promisify(client.hlen).bind(client);
    const ret = await commandAsync(args[0]);
    finalResult.push(Number(ret));
};