const {promisify} = require('util');
module.exports = async function getCommand(client, finalResult, args) {
    const commandAsync = promisify(client.get).bind(client);
    const ret = await commandAsync(args[0]);
    finalResult.push(ret);
};