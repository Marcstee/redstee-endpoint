const {promisify} = require('util');
module.exports = async function llenCommand(client, finalResult, args) {
    const commandAsync = promisify(client.llen).bind(client);
    const ret = await commandAsync(args[0]);
    finalResult.push(Number(ret));
};