const {promisify} = require('util');
module.exports = async function hsetCommand(client, finalResult, args) {
    const commandAsync = promisify(client.hset).bind(client);
    const key = args[0];
    const field = args[1];
    const value = args[2];
    const ret = await commandAsync(key, field, value);
    finalResult.push(true);
};