const {promisify} = require('util');
module.exports = async function saddCommand(client, finalResult, args) {
    const commandAsync = promisify(client.sadd).bind(client);
    const key = args[0];
    const value = args[1];
    const ret = await commandAsync(key, value);
    finalResult.push(true);
};