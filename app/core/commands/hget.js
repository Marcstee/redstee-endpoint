const {promisify} = require('util');
module.exports = async function hgetCommand(client, finalResult, args) {
    const commandAsync = promisify(client.hget).bind(client);
    const key = args[0];
    const field = args[1];
    const ret = await commandAsync(key,field);
    finalResult.push(ret);
};