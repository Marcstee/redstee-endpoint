const {promisify} = require('util');
module.exports = async function zremCommand(client, finalResult, args) {
    const commandAsync = promisify(client.zrem).bind(client);
    const key = args[0];
    const member = args[1];
    const ret = await commandAsync(key,member);
    finalResult.push(ret === 1);
};