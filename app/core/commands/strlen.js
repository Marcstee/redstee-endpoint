const {promisify} = require('util');
module.exports = async function strlenCommand(client, finalResult, args) {
    const commandAsync = promisify(client.strlen).bind(client);
    const ret = await commandAsync(args[0]);
    finalResult.push(Number(ret));
};