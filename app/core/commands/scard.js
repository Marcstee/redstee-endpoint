const {promisify} = require('util');
module.exports = async function scardCommand(client, finalResult, args) {
    const commandAsync = promisify(client.scard).bind(client);
    const ret = await commandAsync(args[0]);
    finalResult.push(Number(ret));
};