const {promisify} = require('util');
module.exports = async function delCommand(client, finalResult, args) {
    const commandAsync = promisify(client.del).bind(client);
    const ret = await commandAsync(args[0]);
    finalResult.push(ret === 1);
};