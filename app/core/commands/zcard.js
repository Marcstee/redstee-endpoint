const {promisify} = require('util');
module.exports = async function zcardCommand(client, finalResult, args) {
    const commandAsync = promisify(client.zcard).bind(client);
    const ret = await commandAsync(args[0]);
    finalResult.push(Number(ret));
};