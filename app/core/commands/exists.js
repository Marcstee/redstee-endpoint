const {promisify} = require('util');
module.exports = async function existsCommand(client, finalResult, args) {
    const existsAsync = promisify(client.exists).bind(client);
    const ret = await existsAsync(args[0]);
    finalResult.push(ret !== 0);
};