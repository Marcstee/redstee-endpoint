const {promisify} = require('util');
module.exports = async function typeCommand(client, finalResult, args) {
    const commandAsync = promisify(client.type).bind(client);
    const ret = await commandAsync(args[0]);
    finalResult.push(ret);
};