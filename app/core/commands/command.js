const {promisify} = require('util');
module.exports = async function commandCommand(client, finalResult, args) {
    const commandAsync = promisify(client.send_command).bind(client);
    const comm = args.shift();
    try {
        let ret = await commandAsync(comm, [...args]);
        if (typeof ret === "string") {
            ret = ret.split('\n');
        }
        finalResult.push(ret);
    } catch (e) {
        finalResult.push(['invalid command']);
    }
};