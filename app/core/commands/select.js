const {promisify} = require('util');
module.exports = async function selectCommand(client, finalResult, args) {
    const commandAsync = promisify(client.select).bind(client);
    await commandAsync(args[0]);
    finalResult.push([]);
};