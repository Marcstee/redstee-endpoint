const {promisify} = require('util');
module.exports = async function ttlCommand(client, finalResult, args) {
    const commandAsync = promisify(client.ttl).bind(client);
    const ret = await commandAsync(args[0]);
    finalResult.push(ret);
};