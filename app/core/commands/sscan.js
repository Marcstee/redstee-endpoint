const {promisify} = require('util');
module.exports = async function sscanCommand(client, finalResult, args) {
    const commandAsync = promisify(client.sscan).bind(client);

    let cursor = null;
    const key = args[0];
    const match = args[1];
    const maxItems = args[2];
    const count = 10000;
    let keys = [];
    let keysSet = new Set();
    let isMore = false;

    async function doScan() {
        if (cursor === null) {
            cursor = '0';
        }
        const ret = await commandAsync(key, cursor, 'MATCH', match, 'COUNT', count);
        cursor = ret[0];
        return ret[1];
    }

    while (keysSet.size <= maxItems && cursor !== '0') {
        const scannedKeys = await doScan();
        scannedKeys.forEach(key => {
            keysSet.add(key);
        });
    }
    keys = [...keysSet];
    if (cursor !== '0'){
        isMore = true;
    }
    if  (keys.length > maxItems){
        keys = keys.slice(0,maxItems - 1);
        isMore = true;
    }
    finalResult.push({
        list: keys,
        isMore
    });
};

