const {promisify} = require('util');
module.exports = async function zcountCommand(client, finalResult, args) {
    const key = args[0];
    const min = args[1];
    const max = args[2];
    const commandAsync = promisify(client.zcount).bind(client);
    const ret = await commandAsync(key, min, max);
    finalResult.push(Number(ret));
};