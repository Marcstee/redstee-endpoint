const {promisify} = require('util');
module.exports = async function expireatCommand(client, finalResult, args) {
    const commandAsync = promisify(client.expireat).bind(client);
    const key = args[0];
    const timestamp = args[1];
    const ret = await commandAsync(key, timestamp);
    finalResult.push(ret === 1);
};