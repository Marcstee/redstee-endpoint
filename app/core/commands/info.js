const {promisify} = require('util');
module.exports = async function infoCommand(client, finalResult, args) {
    const commandAsync = promisify(client.info).bind(client);
    const infoArray = {};
    let infoData = await commandAsync(args[0]);
    infoData = infoData.split("\n");
    infoData = infoData.slice(1, infoData.length - 1);
    infoData.map((info) => {
        const infoParts = info.replace('\r', '').split(':');
        infoArray[infoParts[0]] = infoParts[1];
    });
    finalResult.push(infoArray);
};