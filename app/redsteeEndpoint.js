'use strict';
const express = require('express');
const bodyParser = require('body-parser');

const endPointInfo = require('./core/endpoint-info');
const commands = require('./core/commands');

// Constants
const PORT = 3777;
const HOST = '0.0.0.0';

// App
const app = express();
app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.get('/',endPointInfo);
app.post('/',commands);

app.listen(PORT, HOST);
console.log(`Running on http://${HOST}:${PORT}`);
