'use strict';
const express = require('express');
const bodyParser = require('body-parser');
const https = require('https');
const fs = require('fs');

const endPointInfo = require('./core/endpoint-info');
const commands = require('./core/commands');

// App
const app = express();
app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.get('/',endPointInfo);
app.post('/',commands);

/**
 * Provide cert files path
 */
const privateKey = fs.readFileSync('./cert/priv.pem', 'utf8');
const certificate = fs.readFileSync('./cert/crt.pem', 'utf8');
const ca = fs.readFileSync('./cert/intermediate.pem', 'utf8');

const credentials = {
    key: privateKey,
    cert: certificate,
    ca: ca
};
const httpsServer = https.createServer(credentials, app);
httpsServer.listen(443, () => {
    console.log('HTTPS Server running on port 443');
});